package ipa150.pe;

public class ItemAttribute {
	private String name;
	private String value;
	private String type;
	
	public ItemAttribute(String name, String value, String type) {
		super();
		this.name = name;
		this.value = value;
		this.type = type;
	}

	public ItemAttribute() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Object getValue() {
		switch (type){ 
			case "INTEGER":
				return Integer.parseInt(value);	
			case "FLOAT":
				return Float.parseFloat(value);
			case "BOOLEAN":
				return Boolean.parseBoolean(value);
			case "STRING":
				return value;
		}
			
		return false;
		
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
	
	
}
