package ipa150.pe;

import java.time.LocalDateTime;
import java.util.HashMap;

import org.kie.api.KieBaseConfiguration;
import org.kie.api.KieServices;
import org.kie.api.builder.ReleaseId;
import org.kie.api.conf.EventProcessingOption;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

public class RulesLoader {

	private HashMap<String, LocalDateTime> lastFired = new HashMap<String, LocalDateTime>();
	
    public static final void main(String[] args) {
        try {
            // load up the knowledge base
        	KieServices kieServices = KieServices.Factory.get();
        	ReleaseId releaseId = kieServices.newReleaseId( "ipa150.pe", "RulesLoader", "0.0.1-SNAPSHOT");
        	//KieContainer kc = kieServices.newKieContainer( releaseId );
        	//KieScanner kScanner = kieServices.newKieScanner( kContainer );
        	KieContainer kContainer = kieServices.getKieClasspathContainer();
        	//KieContainer kContainer = kieServices.newKieContainer(releaseId);
        	KieSession kSession = kContainer.newKieSession("loader");
        	System.out.println("release: "+kContainer.getReleaseId());
        	
        	CommonBaseEvent event = new CommonBaseEvent();
        	event.setIdentifier(542706693);
        	ItemAttribute attribute = new ItemAttribute("Maintenance","false","STRING");
        	event.addToAttributes(attribute);
        	event.setSender("verschleiss");
        	//CommonBaseEvent event2 = event;
        	
        	kSession.insert(event);
        	kSession.fireAllRules();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
    
	public Boolean canBeFired(String ruleName, int minutes){
		LocalDateTime time = LocalDateTime.now();
		LocalDateTime last;
		System.out.println("CanBeFired!!");
		if (lastFired.containsKey(ruleName)){
			last = lastFired.get(ruleName);
			if (time.minusMinutes(minutes).isAfter(last)){
				lastFired.put(ruleName, time);
				return true;
			} else {
				return false;
			}
		} else {
			lastFired.put(ruleName, time);
			return true;
		}
	}

}
