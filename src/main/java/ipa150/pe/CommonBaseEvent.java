package ipa150.pe;

import java.time.*;
import java.util.HashMap;
import java.util.Map;

public class CommonBaseEvent {
	private int identifier;
	private HashMap<String, ItemAttribute> attributes;
	private LocalDateTime occured;
	private String sender;
	
	public CommonBaseEvent(){
		this.attributes = new HashMap<String, ItemAttribute>();
	}
	
	public CommonBaseEvent(int identifier, HashMap<String, ItemAttribute> attributes, LocalDateTime occured, String sender){
		super();
		this.identifier = identifier;
		this.attributes = attributes;
		this.occured = occured;
		this.sender = sender;
	}
	
	public int getIdentifier() {
		return identifier;
	}
	
	public void setIdentifier(int identifier) {
		this.identifier = identifier;
	}
	
	public LocalDateTime getOccured() {
		return occured;
	}
	
	public void setOccured(LocalDateTime occured) {
		this.occured = occured;
	}
	
	public String getSender() {
		return sender;
	}
	
	public void setSender(String sender) {
		this.sender = sender;
	}
	
	public Map<String, ItemAttribute> getAttributes() {
		return attributes;
	}
	
	public void addToAttributes(ItemAttribute attribute) {
		//System.out.println(attribute.getName());
		//System.out.println(attribute);
		this.attributes.put(attribute.getName(), attribute);
	}
	
	public ItemAttribute getAttributeByName (String name){
		return attributes.get(name);
	}
}